<?php
  $page_title = 'Agregar venta';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
  $all_categories = find_all('categories');
  $all_products = find_all('products');
?>
<?php
 if(isset($_POST['add_sale'])){
   $req_fields = array('s_id','quantity','price' );
   validate_fields($req_fields);
   if(empty($errors)){
     $p_id  = remove_junk($db->escape($_POST['s_id']));
     $p_qty   = remove_junk($db->escape($_POST['quantity']));
     $p_total   = remove_junk($db->escape($_POST['price']));

     $date    = make_date();
     $query  = "INSERT INTO sales (";
     $query .=" product_id,qty,price,date";
     $query .=") VALUES (";
     $query .=" '{$p_id}', '{$p_qty}', '{$p_total}', '{$date}'";
     $query .=")";

     if($db->query($query)){
       $session->msg('s',"Producto agregado exitosamente. ");
       redirect('add_sale.php', false);
     } else {
       $session->msg('d',' Lo siento, registro falló.');
       redirect('sale.php', false);
     }

   } else{
     $session->msg("d", $errors);
     redirect('add_sale.php',false);
   }

 }

?>

?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
  <div class="col-md-12">
    <?php echo display_msg($msg); ?>

  </div>
</div>
  <div class="row">
  <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Agregar venta</span>
         </strong>
        </div>
        <div class="panel-body">
         <div class="col-md-12">
          <form method="post" action="add_sale.php" class="clearfix">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <select class="form-control" name="s_id">
                      <option value="">Selecciona una categoría</option>
                    <?php  foreach ($all_categories as $cat): ?>
                      <option value="<?php echo (int)$cat['id'] ?>">
                        <?php echo $cat['name'] ?></option>
                    <?php endforeach; ?>
                    </select>
                  </div>
                    <div class="col-md-4">
                    <select class="form-control" name="s_id">
                      <option value="">Seleccione un producto</option>
                    <?php  foreach ($all_products as $cat): ?>
                      <option value="<?php echo (int)$cat['id'] ?>">
                        <?php echo $cat['name'] ?></option>
                    <?php endforeach; ?>
                    </select>
                  </div>
                 <div class="col-md-4">
                   <div class="input-group">
                     <span class="input-group-addon">
                      <i class="glyphicon glyphicon-shopping-cart"></i>
                     </span>
                     <input type="number" class="form-control" name="quantity" placeholder="Cantidad">
                  </div>
                 </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                <div class="col-md-6">
                   <div class="input-group">
                     <span class="input-group-addon">
                       <i class="glyphicon glyphicon-usd"></i>
                     </span>
                     <input type="number" class="form-control" name="price" placeholder="Precio de compra">
                     <span class="input-group-addon">.00</span>
                  </div>
                 </div>
                  <div class="col-md-6">
                   <div class="input-group">
                     <span class="input-group-addon">
                       <i class="glyphicon glyphicon-usd"></i>
                     </span>
                     <input type="number" class="form-control disabled" name="total" placeholder="Total de compra">
                     <span class="input-group-addon">.00</span>
                  </div>
                 </div>
                
                </div>
                </div>
              <div class="form-group">
               <div class="row">
               </div>
              </div>
              <button type="submit" name="add_sale" class="btn btn-danger">Agregar venta</button>
          </form>
         </div>
        </div>
      </div>
    </div>
  </div>

<!--
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
    <form method="post" action="ajax.php" autocomplete="off" id="sug-form">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary">Búsqueda</button>
            </span>
            <input type="text" id="sug_input" class="form-control" name="title"  placeholder="Buscar por el nombre del producto">
         </div>
         <div id="result" class="list-group"></div>
        </div>
    </form>
  </div>
</div>
<div class="row">

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Editar venta</span>
       </strong>
      </div>
      <div class="panel-body">
        <form method="post" action="add_sale.php">
         <table class="table table-bordered">
           <thead>
            <th> Producto </th>
            <th> Precio </th>
            <th> Cantidad </th>
            <th> Total </th>
            <th> Agregado</th>
            <th> Acciones</th>
           </thead>
            <td>
                <input type="number" class="form-control" name="" placeholder="Cantidad">
            </td>            <td>
                <input type="number" class="form-control" name="" placeholder="Cantidad">
            </td>            <td>
                <input type="number" class="form-control" name="" placeholder="Cantidad">
            </td>            <td>
                <input type="number" class="form-control" name="" placeholder="Cantidad">
            </td>            <td>
                <input type="number" class="form-control" name="" placeholder="Cantidad">
            </td>
            <td><input type="number" class="form-control" name="" placeholder="Precio de compra">
            </td>
             <tbody  id="product_info"> </tbody>
         </table>
       </form>
      </div>
    </div>
  </div>

</div>
-->
<?php include_once('layouts/footer.php'); ?>
